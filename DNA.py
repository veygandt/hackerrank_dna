from collections import deque

SIZE_ALPHABET = 26

tree = []

def input_benefits_gen_list():
    num_of_gen = int(input())
    gens_name = input().split()
    gens_weights = list(map(int, input().split()))
    return num_of_gen, gens_name, gens_weights


def input_strands():
    strands = []
    num_of_strands = int(input())
    for i in range(num_of_strands):
        start, stop, strand = input().split()
        strand_dict = {"start": start, "stop": stop, "strand": strand}
        strands.append(strand_dict)
    return strands


# noinspection PyPep8Naming
def DNA_count_sample(gens_name, gens_weights, len_gens, strand_max, strand_min, strands):
    for strand in strands:
        #        print(f"strand={strand}")
        cur_strand_sum = 0
        #        print(f'gen_index={range(int(strand["start"]),int(strand["stop"]))}')
        for gen_index in range(int(strand["start"]), int(strand["stop"]) + 1):
            #            print(f"gen={gens_name[gen_index]} {gens_weights[gen_index]} {gen_index} ")
            start_ind = 0
            while (start_ind + len_gens[gen_index]) <= len(strand["strand"]):
                #                print(f'strand[start_ind:]={ strand["strand"][start_ind:]}')
                if strand["strand"][start_ind:].startswith(gens_name[gen_index]):
                    cur_strand_sum += gens_weights[gen_index]
                #                    print(f"popali pos={start_ind} ves={gens_weights[gen_index]}")
                start_ind += 1
        #        print(f"sum = {cur_strand_sum}")
        if strand_max is None:
            strand_max = cur_strand_sum
        #            print(f"strand_max ={strand_max }")
        if cur_strand_sum > strand_max:
            strand_max = cur_strand_sum
        if strand_min is None:
            strand_min = cur_strand_sum
        if cur_strand_sum < strand_min:
            strand_min = cur_strand_sum
    return strand_max, strand_min


class Node:
    def __init__(self, suff_link = None, char= None, parent = None ):
       # self.index = None
        self.end_node = False
        self.auto_move = [None]*SIZE_ALPHABET
        self.weight = []
        self.next_node = [None]*SIZE_ALPHABET
        self.suff_link = 0
        self.suff_flink = 0
        self.char = char   #символ на ребре от par к этой вершине
        self.parent = parent


def add_string_to_bohr(gen, tree, weight=0):
    ord_a = ord('a')
    num = 0  # начинаем с корня
    for char in gen:
        char_code = ord(char) - ord_a
        if tree[num].next_node[char_code] is None:
            tree.append(Node(suff_link = 0, char= char, parent = tree[num]))
            tree[num].next_node[char_code] = len(tree) - 1
        num = tree[num].next_node[char_code]
    tree[num].end_node = True
    tree[num].weight.append( weight)


def build_tree(gen_list, gens_weights):
    root_node = Node()
    #tree = \
    tree.append(root_node)
    for i, gen in enumerate(gen_list):
        add_string_to_bohr(gen, tree, gens_weights[i])
    return tree


def set_suffix_links():
    q = deque()
    child = 0
    for node in tree[0].next_node:
        if node:
            q.append(node)
            tree[node].suff_link = 0
            while q:
                r = q.popleft()
                for child in tree[r].next_node:
                    if child:
                        q.append(child)
                        state = tree[r].suff_link
                        if state is None:
                            print(f"suff_link=None")
                        while find_next_state(state, tree[child].char) == None and state != 0:
                            state = tree[state].suff_link
                        suff_link = find_next_state(state, tree[child].char)
                        tree[child].suff_link = suff_link if suff_link else 0
                if child and tree[child].suff_link is None:
                    tree[child].suff_link = 0
                if child:
                    tree[child].weight = tree[child].weight + tree[tree[child].suff_link].weight
    print("end suffix link settings")


def find_next_state(current_state, value):
    char_code = ord(value) - ord('a')
    if tree[current_state].next_node[char_code]:
        return tree[current_state].next_node[char_code]
    return None


def is_string_in_bohr2(string, tree) -> bool:
    current_state = 0
    ord_a = ord('a')
    _sum = 0
    keywords_found =[]
    for i, char in enumerate(string):
        char_code = ord(char) - ord_a
        print(f"char_code={char_code}")
        print(f"tree = {tree[current_state].next_node[char_code]}")
        while current_state != 0 and find_next_state(current_state, char) is None :
            current_state = tree[current_state].suff_link
            if not current_state:
                break
        current_state = find_next_state(current_state, char)
        if current_state is None:
            current_state = 0
        else:
            #for j in tree[current_state].weight:
            if tree[current_state].weight:
                _sum+= sum(tree[current_state].weight)
    return _sum


def is_string_in_bohr(string, tree) -> bool:
    num = 0
    ord_a = ord('a')
    _sum = 0
    for char in string:
        char_code = ord(char) - ord_a
        print(f"char_code={char_code}")
        print(f"tree = {tree[num].next_node[char_code]}")
        if tree[num].end_node:
            _sum += tree[num].weight
        if tree[num].next_node[char_code] is None:
            print(f"=====sum={_sum}")
            return False

        num = tree[num].next_node[char_code]
        print(f"next num={num}")

    return True


# noinspection PyPep8Naming
def DNA_count_AHO(gens_name, gens_weights, len_gens, strand_max, strand_min, strands):
    for strand in strands:
        #        print(f"strand={strand}")
        cur_strand_sum = 0
        #        print(f'gen_index={range(int(strand["start"]),int(strand["stop"]))}')
        for gen_index in range(int(strand["start"]), int(strand["stop"]) + 1):
            #            print(f"gen={gens_name[gen_index]} {gens_weights[gen_index]} {gen_index} ")
            start_ind = 0
            while (start_ind + len_gens[gen_index]) <= len(strand["strand"]):
                #                print(f'strand[start_ind:]={ strand["strand"][start_ind:]}')
                if strand["strand"][start_ind:].startswith(gens_name[gen_index]):
                    cur_strand_sum += gens_weights[gen_index]
                #                    print(f"popali pos={start_ind} ves={gens_weights[gen_index]}")
                start_ind += 1
        #        print(f"sum = {cur_strand_sum}")
        if strand_max is None:
            strand_max = cur_strand_sum
        #            print(f"strand_max ={strand_max }")
        if cur_strand_sum > strand_max:
            strand_max = cur_strand_sum
        if strand_min is None:
            strand_min = cur_strand_sum
        if cur_strand_sum < strand_min:
            strand_min = cur_strand_sum
    return strand_max, strand_min


def main2():
    num_of_gen, gens_name, gens_weights = input_benefits_gen_list()
    strands = input_strands()
    len_gens = []
    for gen in gens_name:
        len_gens.append(len(gen))
    strand_max = None
    strand_min = None
#    print(f"gens_name={gens_name}")
    strand_max, strand_min = DNA_count_sample(gens_name, gens_weights, len_gens, strand_max, strand_min, strands)

#        print("======================")
    print(strand_min, strand_max)


def main():
    gens_weights = [1, 2, 2, 3, 6,7]
    tree = build_tree(["abcd", "abc", "abf", "rr", "a", "aa"], gens_weights)
    set_suffix_links()
    print(f"result for aabcd = {is_string_in_bohr2('aaabcd', tree)}")
    #print(f"result for abcdaa = {is_string_in_bohr('abcdaaa', tree)}")
    #print(f"result zzz= {is_string_in_bohr('zzz', tree)}")
    #print(f"result rrr= {is_string_in_bohr('aabcd', tree)}")


if __name__ == "__main__":
    main()
